import $ from 'jquery';

$(() => {
  const blocks = document.getElementsByTagName('div');
  Array.from(blocks)
    .forEach(showImages);

});

function showImages(block) {
  const images = block.getElementsByTagName('img');
  Array.from(images).forEach(img => {
    img.style.width = `${img.offsetWidth}px`;
  });
}

