import $ from 'jquery';
import './chosen.jquery.js';
import { rAFProcessList } from '../raf';


$(() => {

  const chosenContainers = $('.js-chosen').toArray();

  rAFProcessList(chosenContainers, (container) => {
    $(container).chosen();
  });

});


// setInterval(() => {
//   console.log(1);
// }, 100);
