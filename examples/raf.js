const hasPerformance = !!(window.performance && window.performance.now);

export const rAF = window.requestAnimationFrame
         || window.webkitRequestAnimationFrame
         || window.mozRequestAnimationFrame
         || window.msRequestAnimationFrame
         || function rAFImitator(func) { return window.setTimeout(func, 1000 / 60); };


// Каждый новый вызов фукнции будет откладывать ее в следующие кадры
export const rAFed = func => () => rAF(func);


// Первый вызов фукнции отложит ее выполнение до следующего raf, остальные вызовы
// будут проигнорированы до тех пор, пока функция не исполнится.
// Inspired by: https://www.html5rocks.com/en/tutorials/speed/animations/
export function rAFDebounce(func) {
  let scheduledFrame = false;
  return () => {
    if (scheduledFrame) return;
    scheduledFrame = true;
    rAF(() => {
      func();
      scheduledFrame = false;
    });
  };
}


// Если есть задача, которая модифицирует DOM и выполняется долго, то лучше ее разбить на
// много маленьких и делать их в рамках requestAnimationFrame.
// https://developers.google.com/web/fundamentals/performance/rendering/optimize-javascript-execution
export function rAFProcessList(dataList, fn) {
  if (!dataList.length) return;

  function processTask(startTime) {
    const nextData = dataList.pop();
    fn(nextData);
    const finishTime = window.performance.now();

    if (!dataList.length) return;

    // если задача уложиласть в 3ms, можно в этот кадр еще одну впихнуть
    if (finishTime - startTime < 3) {
      processTask(finishTime);
    } else {
      rAF(processTask);
    }
  }

  if (hasPerformance) {
    rAF(processTask);
  } else {
    // в глупых браузерах разбиваем задачи на циклы event loop'а,
    // чтобы браузер колом не вставал хотя бы
    dataList.forEach(data => window.setTimeout(() => fn(data), 0));
  }
}
