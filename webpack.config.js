const webpack = require('webpack');
const path = require('path');


module.exports = {
  context: path.resolve(__dirname, 'examples'),
  entry: {
    chunks_bad: './chunks/bad.js',
    chunks_good: './chunks/good.js',
    trashing_bad: './trashing/bad.js',
    trashing_good: './trashing/good.js',
  },
  output: {
    filename: "./examples/_bundles/[name].js"
  },
  module: {
   rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
      }
    ]
  },
  devtool: 'source-map',
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ]
}
